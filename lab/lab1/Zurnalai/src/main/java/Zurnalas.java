/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Klasė skirta informacijai apie Zurnalus saugoti/apdoroti
 * @author kasparas
 */
public class Zurnalas {
    private String pavadinimas;
    private int puslapiuSkaicius;
    private double kaina;
    
    /**
     * Klasės konstruktorius, su parametrais
     * @param pavadinimas
     * @param puslapiai
     * @param kaina 
     */
    public Zurnalas(String pavadinimas, int puslapiai, double kaina) {
        this.pavadinimas = pavadinimas;
        this.puslapiuSkaicius = puslapiai;
        this.kaina = kaina;
    }
    
    /**
     * Metodas skirtas grazinti zurnalo pavadinima
     * @return 
     */
    public String GautiPavadinima() {
        return this.pavadinimas;
    }
    
    /**
     * Metodas skirtas grazinti puslapiu skaiciu
     * @return 
     */
    public int GautiPuslapius() {
        return this.puslapiuSkaicius;
    }
    
    /**
     * Metodas skirtas grazinti zurnalo kaina
     * @return zurnalo kaina
     */
    public double GautiKaina() {
        return this.kaina;
    }
    
    /**
     * Metodas skirtas grazinti vieno puslapio kaina
     * @return vieno puslapio kaina
     */
    public double GautiPuslaioKaina() {
        return puslapiuSkaicius / kaina;
    }
    
    /**
     * Metodas skirtas nunulinti reiksmes
     */
    public void Nulinti() {
        kaina = 0;
    }
}
