/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Zurnalai
 * @author kasparas
 */

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class main {
    static private int Cn = 100;
    static String duomenys1 = "duomenys1.txt";
    static String duomenys2 = "duomenys2.txt";
    static String pradiniai = "pradiniai.txt";
    static String rezultatai =  "rezultatai.txt";

    /**
     * Metodas skirtas nuskaityti faila turinti duomenis apie zurnalus
     * @param zurnal zurnalu sarasa
     * @param failoVieta failo vieta
     * @throws Exception
     */
    public String Skaityti(List <Zurnalas> zurnal, String failoVieta)
            throws Exception{
        String eilute;
        
        BufferedReader skaitytojas = new BufferedReader(
                new InputStreamReader(new FileInputStream(failoVieta)));
        
        String salis = skaitytojas.readLine();
        while ((eilute = skaitytojas.readLine()) != null ) {
            String[] dalys = eilute.split(" ");
            String pavadinimas = dalys[0];
            int puslapiai = Integer.parseInt(dalys[1]);
            double kaina = Double.parseDouble(dalys[2]);
            Zurnalas laik = new Zurnalas(pavadinimas, puslapiai, kaina);
            zurnal.add((laik));
        }
        skaitytojas.close();
        
        if (salis != null && !salis.isEmpty()) 
            return salis;
        return null;
    }
        
    /**
     * Metodas skirtas surasti knyga su didžiausia kaina
     * @param zurnal Zurnalu sarasas
     * @return Didziausios kainos indeksas
     */
    public int RastiDidziausiaKaina(List <Zurnalas> zurnal)
    {
        int didziausias = 0;
        for (int tikrininamas = 1; tikrininamas < zurnal.size(); tikrininamas++)
            if (zurnal.get(tikrininamas).GautiPuslaioKaina() >
                zurnal.get(didziausias).GautiPuslaioKaina())
                didziausias = tikrininamas;

        return didziausias;
    }
    
    /**
     * Metodas skirtas surasti bendra visu zurnalu puslapiu suma
     * @param zurnal Zurnalu objektu masyvas
     * @return puslapiu suma
     */
    public int RastiPuslapiuSuma(List <Zurnalas> zurnal)
    {
        int suma = 0;
        for (int tikrininamas = 0; tikrininamas < zurnal.size(); tikrininamas++)
            suma += zurnal.get(tikrininamas).GautiPuslapius();

        return suma;
    }
    
    /**
     * Metodas skirtas surasti vidutine kainą
     * @param zurnal Zurnalu objektu sarasas
     * @param puslapKiekis Puslapiu kiekis
     * @return 
     */
    public double RastiVidutineKaina(List <Zurnalas> zurnal, int puslapKiekis) {
        double suma = 0;
        for (int tikrininamas = 0; tikrininamas < zurnal.size(); tikrininamas++)
            suma += zurnal.get(tikrininamas).GautiKaina();

        return suma / puslapKiekis;
    }
    
    /**
     * Metodas skirtas surasti kurioje valstybeje zurnalo puslapio kaina 
     * didziausia
     * @param salis1Kaina
     * @param salis2Kaina
     * @param pav1
     * @param pav2
     * @return pavadinimas salies/-u kur zurnalo puslap. kaina didziausia 
     */
    public String RastiBrangiausiaSali(double salis1Kaina,
       double salis2Kaina, String pav1, String pav2)
    {
        if (salis1Kaina == salis2Kaina)
            return pav1 + " ir " + pav2;
        else if (salis1Kaina > salis2Kaina)
            return pav1;
        else
            return pav2;
    }

    /**
     * Metodas skirtas spausdinti klasėje išsaugotus duomenis
     * @param zurnal zurnalu sarasas
     * @param pavadinimas lenteles pavadinimas
     * @throws Exception 
     */
    public void SpausdintiDuomenis(List <Zurnalas> zurnal, String pavadinimas,
            String failoKelias) throws Exception {
        BufferedWriter rasytojas = new BufferedWriter(
                new FileWriter(failoKelias, true));

        rasytojas.write("|--------------------------------------------------" +
            "--------------------------|\r\n");
        rasytojas.write(String.format("|                 %25s               " +
            "                   |\r\n", pavadinimas));
        rasytojas.write("|--------------------------------------------------" +
            "--------------------------|\r\n");
        rasytojas.write("|        Pavadinimas       |      Puslapių skaičius" +
            "    |         Kaina       |\r\n");
        rasytojas.write("|                          |                     " +
            "      |                     |\r\n");
        for (int x = 0; x < zurnal.size(); x++)
        {
            rasytojas.write(String.format("|        %-14s    | %16d          |"+
                " %15f     |\r\n", zurnal.get(x).GautiPavadinima(),
                zurnal.get(x).GautiPuslapius(), zurnal.get(x).GautiKaina()));
        }
        rasytojas.write("|------------------------------------------------" +
            "----------------------------|\r\n\n");
        rasytojas.close();
    }
    
    /**
     * Metodas skirtas spausdinti galutiniams duomenims
     * @param dydziausiaKaina1
     * @param dydziausiaKaina2
     * @param puslapiusuma1
     * @param puslapiusuma2
     * @param vidutineKaina1
     * @param vidutineKaina2
     * @param dydziausiaPuslapio
     * @param dydziausiaVidutine 
     * @param failoKelias kelias i galutini faila
     * @throws Exception 
     */
    public void SpausdintiGalutinius(String dydziausiaKaina1, 
            String dydziausiaKaina2, int puslapiusuma1,
            int puslapiusuma2, double vidutineKaina1,
            double vidutineKaina2, String dydziausiaPuslapio,
            String dydziausiaVidutine, String failoKelias) throws Exception {
        BufferedWriter rasytojas =
                new BufferedWriter(new FileWriter(failoKelias, true));
        
        rasytojas.write("|------------------------------------------------" +
            "----------------------------|\r\n");
        rasytojas.write(String.format("|  1 šalies žurnalas turintis "
                + "didžiausią kainą:  %22s      |\r\n", dydziausiaKaina1));
        rasytojas.write(String.format("|  2 šalies žurnalas turintis "
                + "didžiausią kainą:  %22s      |\r\n", dydziausiaKaina2));
        rasytojas.write("|------------------------------------------------" +
            "----------------------------|\r\n");
        rasytojas.write(String.format("|  1 šalies žurnalų puslapių kiekis:"
                + "             %22d      |\r\n", puslapiusuma1));
        rasytojas.write(String.format("|  2 šalies žurnalų puslapių kiekis:"
                + "             %22d      |\r\n", puslapiusuma2));
        rasytojas.write("|------------------------------------------------" +
            "----------------------------|\r\n");
        rasytojas.write(String.format("|  1 šalies vidutinė žurnalo puslapio"
                + " kaina:     %22s      |\r\n", vidutineKaina1));
        rasytojas.write(String.format("|  2 šalies vidutinė žurnalo puslapio"
                + " kaina:     %22s      |\r\n", vidutineKaina2));
        rasytojas.write("|--------------------------------------------------" +
            "--------------------------|\r\n");
        rasytojas.write(String.format("|  Šalis su didžiausia puslapio kaina:"
                + "             %20s      |\r\n", dydziausiaPuslapio));
        rasytojas.write(String.format("|  Šalis su didžiausia vidutine"
                + " puslapio kaina:    %20s      |\r\n", dydziausiaVidutine));
        rasytojas.write("|--------------------------------------------------" +
            "--------------------------|\r\n");
        rasytojas.close();
    }
    
    public static void main(String[] args) throws Exception {
        
        main obj = new main();
        
        //1 salis
        List<Zurnalas> salis1 = new ArrayList<>();
        String salis1pav = obj.Skaityti(salis1, duomenys1);
        int dydziausiaKaina1 = obj.RastiDidziausiaKaina(salis1);
        int puslapiusuma1 = obj.RastiPuslapiuSuma(salis1);
        double vidutineKaina1 = obj.RastiVidutineKaina(salis1, puslapiusuma1);
        
        
        //2 salis
        List<Zurnalas> salis2 = new ArrayList<>();
        String salis2pav = obj.Skaityti(salis2, duomenys2);
        int dydziausiaKaina2 = obj.RastiDidziausiaKaina(salis2);
        int puslapiusuma2 = obj.RastiPuslapiuSuma(salis2);
        double vidutineKaina2 = obj.RastiVidutineKaina(salis2, puslapiusuma2);
        
        //Spausdinam pradinius duomenis
        File file = new File(pradiniai);
        file.delete();
        file.createNewFile();
        obj.SpausdintiDuomenis(salis1, salis1pav, pradiniai);
        obj.SpausdintiDuomenis(salis2, salis2pav, pradiniai);

        //Randa valstybe su didziausia puslapio kaina
        String didziausiaPuslapio = obj.RastiBrangiausiaSali(
                salis1.get(dydziausiaKaina1).GautiPuslaioKaina(),
                salis2.get(dydziausiaKaina2).GautiPuslaioKaina(),
                salis1pav, salis2pav);
        
        //Sukurti atskira sarasa
        List<Zurnalas> visosSalys = new ArrayList<>();
        visosSalys.addAll(salis1);
        visosSalys.addAll(salis2);
        
        //Spausfina Galutinius
        File file2 = new File(rezultatai);
        file2.delete();
        file2.createNewFile();
        obj.SpausdintiGalutinius(
                salis1.get(dydziausiaKaina1).GautiPavadinima(),
                salis2.get(dydziausiaKaina2).GautiPavadinima(),
                puslapiusuma1, puslapiusuma2, vidutineKaina1, vidutineKaina2,
                didziausiaPuslapio, didziausiaPuslapio, rezultatai);
    }
}
