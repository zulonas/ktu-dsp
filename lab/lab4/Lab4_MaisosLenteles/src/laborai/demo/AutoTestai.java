package laborai.demo;

import laborai.studijosktu.MapKTUx;
import laborai.studijosktu.Ks;
import java.util.Locale;
import laborai.studijosktu.HashType;

public class AutoTestai {

    public static void main(String[] args) {
        Locale.setDefault(Locale.US); // suvienodiname skaičių formatus
        atvaizdzioTestas();
        //greitaveikosTestas();
    }

    public static void atvaizdzioTestas() {
        Telefonas a1 = new Telefonas("Renault", "Laguna", 1997, 50000, 1700);
        Telefonas a2 = new Telefonas("Renault", "Megane", 2001, 20000, 3500);
        Telefonas a3 = new Telefonas("Toyota", "Corolla", 2001, 20000, 8500.8);
        Telefonas a4 = new Telefonas("Renault Laguna 2001 115900 7500");
        Telefonas a5 = new Telefonas.Builder().buildRandom();
        Telefonas a6 = new Telefonas("Honda   Civic  2007  36400 8500.3");
        Telefonas a7 = new Telefonas("Renault Laguna 2001 115900 7500");

        // Raktų masyvas
        String[] autoId = {"TA156", "TA102", "TA178", "TA171", "TA105", "TA106", "TA107", "TA108"};
        int id = 0;
        MapKTUx<String, Telefonas> atvaizdis
                = new MapKTUx(new String(), new Telefonas(), HashType.DIVISION);
        // Reikšmių masyvas
        Telefonas[] auto = {a1, a2, a3, a4, a5, a6, a7};
        for (Telefonas a : auto) {
            atvaizdis.put(autoId[id++], a);
        }
        atvaizdis.println("Porų išsidėstymas atvaizdyje pagal raktus");
        Ks.oun("Ar egzistuoja pora atvaizdyje?");
        Ks.oun(atvaizdis.contains(autoId[6]));
        Ks.oun(atvaizdis.contains(autoId[7]));
        Ks.oun("Pašalinamos poros iš atvaizdžio:");
        Ks.oun(atvaizdis.remove(autoId[1]));
        Ks.oun(atvaizdis.remove(autoId[7]));
        atvaizdis.println("Porų išsidėstymas atvaizdyje pagal raktus");
        Ks.oun("Atliekame porų paiešką atvaizdyje:");
        Ks.oun(atvaizdis.get(autoId[2]));
        Ks.oun(atvaizdis.get(autoId[7]));
        Ks.oun("Išspausdiname atvaizdžio poras String eilute:");
        Ks.ounn(atvaizdis);
    }

    //Konsoliniame režime
    private static void greitaveikosTestas() {
        System.out.println("Greitaveikos tyrimas:\n");
        GreitaveikosTyrimas gt = new GreitaveikosTyrimas();
        //Šioje gijoje atliekamas greitaveikos tyrimas
        new Thread(() -> gt.pradetiTyrima(),
                "Greitaveikos_tyrimo_gija").start();
        try {
            String result;
            while (!(result = gt.getResultsLogger().take())
                    .equals(GreitaveikosTyrimas.FINISH_COMMAND)) {
                System.out.println(result);
                gt.getSemaphore().release();
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        gt.getSemaphore().release();
    }
}