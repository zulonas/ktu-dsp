package Lab4Zulonas;

import laborai.demo.*;
import laborai.gui.swing.Lab4Window;
import java.util.Locale;

/*
 * Darbo atlikimo tvarka - čia yra pradinė klasė.
 */
public class VykdymoModulis {

    public static void main(String[] args) {
        Locale.setDefault(Locale.US); // Suvienodiname skaičių formatus
        TelTestai.atvaizdzioTestas();
        Lab4Window.createAndShowGUI();
    }
}
