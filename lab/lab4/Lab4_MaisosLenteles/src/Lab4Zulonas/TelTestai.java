package Lab4Zulonas;

import laborai.demo.*;
import laborai.studijosktu.MapKTUx;
import laborai.studijosktu.Ks;
import java.util.Locale;
import laborai.studijosktu.HashType;

public class TelTestai {

    public static void main(String[] args) {
        Locale.setDefault(Locale.US); // suvienodiname skaičių formatus
//        atvaizdzioTestas();
        //greitaveikosTestas();
    }

    public static void atvaizdzioTestas() {
        Telefonas a1 = new Telefonas.Builder().buildRandom();
        Telefonas a2 = new Telefonas.Builder().buildRandom();
        Telefonas a3 = new Telefonas.Builder().buildRandom();
        Telefonas a4 = new Telefonas.Builder().buildRandom();
        Telefonas a5 = new Telefonas.Builder().buildRandom();
        Telefonas a6 = new Telefonas.Builder().buildRandom();
        Telefonas a7 = new Telefonas.Builder().buildRandom();

        // Raktų masyvas
        String[] telId = {"TEL156", "TEL102", "TEL178", "TEL171", "TEL105", "TEL106", "TEL107", "TEL108"};
        int id = 0;
        MapKTUx<String, Telefonas> atvaizdis
                = new MapKTUx(new String(), new Telefonas(), HashType.DIVISION);    
        
        // Reikšmių masyvas
        Telefonas[] auto = {a1, a2, a3, a4, a5, a6, a7};
        for (Telefonas a : auto) {
            atvaizdis.put(telId[id++], a);
        }
        atvaizdis.println("Porų išsidėstymas atvaizdyje pagal raktus");
        Ks.oun("Ar egzistuoja pora atvaizdyje?");
        Ks.oun(atvaizdis.contains(telId[6]));
        Ks.oun(atvaizdis.contains(telId[7]));
        Ks.oun("Pašalinamos poros iš atvaizdžio:");
        Ks.oun(atvaizdis.remove(telId[1]));
        Ks.oun(atvaizdis.remove(telId[7]));
        atvaizdis.println("Porų išsidėstymas atvaizdyje pagal raktus");
        Ks.oun("Atliekame porų paiešką atvaizdyje:");
        Ks.oun(atvaizdis.get(telId[2]));
        Ks.oun(atvaizdis.get(telId[7]));
        Ks.oun("Išspausdiname atvaizdžio poras String eilute:");
        Ks.ounn(atvaizdis);
    }

    //Konsoliniame režime
    private static void greitaveikosTestas() {
        System.out.println("Greitaveikos tyrimas:\n");
        GreitaveikosTyrimas gt = new GreitaveikosTyrimas();
        //Šioje gijoje atliekamas greitaveikos tyrimas
        new Thread(() -> gt.pradetiTyrima(),
                "Greitaveikos_tyrimo_gija").start();
        try {
            String result;
            while (!(result = gt.getResultsLogger().take())
                    .equals(GreitaveikosTyrimas.FINISH_COMMAND)) {
                System.out.println(result);
                gt.getSemaphore().release();
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        gt.getSemaphore().release();
    }
}