/**
 * @author Kasparas Zulonas KTU IFIN-8/3
 */
package Lab3zulonas;

import laborai.studijosktu.BstSetKTU;
import laborai.studijosktu.SetADT;

public class TelApskaita {

    public static SetADT<String> telefonuMarkes(Telefonas[] tel) {
        SetADT<Telefonas> uni = new BstSetKTU<>(Telefonas.pagalGamintoja);
        SetADT<String> kart = new BstSetKTU<>();
        for (Telefonas a : tel) {
            int sizeBefore = uni.size();
            uni.add(a);

            if (sizeBefore == uni.size()) {
                kart.add(a.getGamintojas());
            }
        }
        return kart;
    }
}
