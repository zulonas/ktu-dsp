/**
 * @author Kasparas Zulonas KTU IFIN-8/3
 */
package Lab3zulonas;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.InputMismatchException;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Random;
import laborai.studijosktu.*;

public class Telefonas implements KTUable<Telefonas> {
    final static private int priimtinųMetųRiba = 2014;
    final static private int esamiMetai = LocalDate.now().getYear();
    
    private static final String idCode = "IMEI";    // ***** nauja 
    private static int serNr = 100;                 // ***** naujo
    private final String regNr;
    
    private String gamintojas = "";
    private String modelis = "";
    private int isleidimoMetai = -1;
    private int talpa = -1;
    private double kaina = -1;
    private boolean palaiko5G = false;

    public Telefonas() {
        regNr = idCode + (serNr++);
    }

    public Telefonas(String gamintojas, String modelis,
                    int metai, int talpa, double kaina, boolean palaiko5G) {
        regNr = idCode + (serNr++);
        this.gamintojas = gamintojas;
        this.modelis = modelis;
        this.isleidimoMetai = metai;
        this.talpa = talpa;
        this.kaina = kaina;
        this.palaiko5G = palaiko5G;
        //validate();
    }

    public Telefonas(String dataString) {
        regNr = idCode + (serNr++);
        this.parse(dataString);
    }
    
    public Telefonas(Telefonas.Builder builder) {
        this.regNr = idCode + (serNr++); 
        this.gamintojas = builder.gamintojas;
        this.modelis = builder.modelis;
        this.isleidimoMetai = builder.isleidimoMetai;
        this.talpa = builder.talpa;
        this.kaina = builder.kaina;
        this.palaiko5G = builder.palaiko5g;
        //validate();
    }
                    
    public String getGamintojas() {
        return gamintojas;
    }

    public String getModelis() {
        return modelis;
    }

    public int getIsleidimoMetai() {
        return isleidimoMetai;
    }

    public int getTalpa() {
        return talpa;
    }

    public double getKaina() {
        return kaina;
    }

    public void setKaina(double kaina) {
        this.kaina = kaina;
    }
    
    public boolean getPalaiko5g() {
        return palaiko5G;
    }
    
    public String getRegNr() {
        return regNr;
    }
    
    public int getSerNr() {
        return serNr;
    }

    /**
     * Sukuria naują objektą iš eilutės
     * @param dataString eilutė objekto sukūrimui
     * @return Automobilio klasės objektą
     */
    @Override
    public Telefonas create(String dataString) {
        return new Telefonas(dataString);
    }
    
    /**
     * Suformuoja objektą iš eilutės
     * @param dataString eilutė objektui formuoti
     */
    @Override
    public final void parse(String dataString) {
        try {   
            Scanner ed = new Scanner(dataString); 
            // numatytieji skirtukai: tarpas, tab, eilutės pabaiga
            // Skiriklius galima pakeisti Scanner klasės metodu useDelimitersr 
            // Pavyzdžiui, ed.useDelimiter(", *"); reikštų, kad skiriklis 
            // bus kablelis ir vienas ar daugiau tarpų.
            gamintojas = ed.next();
            modelis = ed.next();
            isleidimoMetai = ed.nextInt();
            talpa = ed.nextInt();
            setKaina(ed.nextDouble());
            palaiko5G = ed.nextBoolean();
        } catch (InputMismatchException e) {
            Ks.ern("Blogas duomenų formatas apie telefoną -> " + dataString);
        } catch (NoSuchElementException e) {
            Ks.ern("Trūksta duomenų apie telefoną -> " + dataString);
        }
    }

    /**
     * Suformuoja objektą iš eilutės
     * @param dataString eilutė objektui formuoti
     */
    public final String parseErr(String dataString) {
        try {   
            Scanner ed = new Scanner(dataString); 
            // numatytieji skirtukai: tarpas, tab, eilutės pabaiga
            // Skiriklius galima pakeisti Scanner klasės metodu useDelimitersr 
            // Pavyzdžiui, ed.useDelimiter(", *"); reikštų, kad skiriklis 
            // bus kablelis ir vienas ar daugiau tarpų.
            gamintojas = ed.next();
            modelis = ed.next();
            isleidimoMetai = ed.nextInt();
            talpa = ed.nextInt();
            setKaina(ed.nextDouble());
            palaiko5G = ed.nextBoolean();
        } catch (InputMismatchException e) {
            return String.format("Blogas duomenų formatas apie telefoną -> "
                    + dataString);
        } catch (NoSuchElementException e) {
            return String.format("Trūksta duomenų apie telefoną -> "
                    + dataString);
        }
        
        return "";
    }

    /**
     * Patikrina objekto reikšmes pagal norimmas taisykles
     * @return tuščią eilutę arba eilutę-klaidos tipą
     */
    @Override
    public String validate() {
        String klaidosTipas = "";
        if (isleidimoMetai < priimtinųMetųRiba || isleidimoMetai > esamiMetai) {
            klaidosTipas = "Netinkami gamybos metai, turi būti [" 
                    + priimtinųMetųRiba + ":" + esamiMetai + "]";
        }
        if (kaina > 1000) {
            klaidosTipas += " Kaina per brangi, turėtų būti: " + (kaina - 1000)
                    + "€ pigesnė";
        }
        return klaidosTipas;
    }

    /**
     * Objekto reikšmių išvedimas, nurodant išvedime tik objekto vardą
     * @return Išvedimui suformuota eilutė
     */
    @Override
    public String toString() {  // surenkama visa reikalinga informacija
        return String.format("%-8s\t %-8s \t %-8s %4d %7d %8.1f %8d %s",
                regNr, gamintojas, modelis, isleidimoMetai, talpa, kaina,
                (palaiko5G == true)? 1: 0, ""/*validate()*/);
    }

    @Override
    public int compareTo(Telefonas a) {
        return getRegNr().compareTo(a.getRegNr());
    }

    /**
     * Rikiavimo pagal modeli komparatorius
     */
    public final static Comparator<Telefonas> pagalGamintoja
            = (Telefonas a1, Telefonas a2) -> {
        int cmp = a1.getGamintojas().compareTo(a2.getGamintojas());
        return cmp;
    };

    /**
     * Rikiavimo pagal kainą komparatorius
     */
    public final static Comparator<Telefonas> pagalKainą
            = (Telefonas o1, Telefonas o2) -> {
                double k1 = o1.getKaina();
                double k2 = o2.getKaina();
                // didėjanti tvarka, pradedant nuo mažiausios
                if (k1 < k2) {
                    return -1;
                }
                if (k1 > k2) {
                    return 1;
                }
                return 0;
    };

    /**
     * Rikiavimo pagal metus ir kainą komparatorius
     */
    public final static Comparator pagalMetusKainą =
            (Comparator) (Object o1, Object o2) -> {
        Telefonas a1 = (Telefonas) o1;
        Telefonas a2 = (Telefonas) o2;
        // metai mažėjančia tvarka, esant vienodiems metams, lyginama kaina
        if(a1.getIsleidimoMetai() < a2.getIsleidimoMetai())
            return 1;
        if(a1.getIsleidimoMetai() > a2.getIsleidimoMetai())
            return -1;
        if(a1.getKaina() < a2.getKaina())
            return 1;
        if(a1.getKaina() > a2.getKaina())
            return -1;
        return 0;
    };

    /**
     * Random telefonų generatorius
     */
    public static class Builder {
        
        private final static Random RANDOM = new Random(1949);
        private final static String[][] MODELIAI = { 
            {"Iphone", "6", "6P", "7", "7P", "X", "XS", "XR"},
            {"Huaway", "P10", "P9", "P20", "P30"},
            {"Samsung", "S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8",
                "S9", "S10"},
            {"Google", "6p", "pixel1", "pixel2", "pixel3", "pixel4"},
        };

        private String gamintojas = "";
        private int isleidimoMetai = -1;
        private double kaina = -1;
        private String modelis = "";
        private int talpa = -1;
        private boolean palaiko5g = false;
        private String idCode = "IMEI";    // ***** nauja 

        public Telefonas build() {
            return new Telefonas(this);
        }

        public Telefonas buildRandom() {
            int ma = RANDOM.nextInt(MODELIAI.length);   
            int mo = RANDOM.nextInt(MODELIAI[ma].length - 1) + 1;
            return new Telefonas(MODELIAI[ma][0],
                    MODELIAI[ma][mo],
                    2007 + RANDOM.nextInt(10), // metai tarp 2007 ir 2017
                    2 + 2*RANDOM.nextInt(3),  // talpa
                    100 + RANDOM.nextDouble() * 2000, //kaina
                    RANDOM.nextBoolean()); // 5g palaikymas
        }
     
        public Builder gamintojas(String gamintojas) {
            this.gamintojas = gamintojas;
            return this;
        }
        
        public Builder isleidMetai(int isleidMetai) {
            this.isleidimoMetai = isleidMetai;
            return this;
        }
        
        public Builder kaina(double kaina) {
            this.kaina = kaina;
            return this;
        }

        public Builder modelis(String modelis) {
            this.modelis = modelis;
            return this;
        }

        public Builder talpa(int talpa) {
            this.talpa = talpa;
            return this;
        }
        
        public Builder palaiko5g(boolean palaiko){
            this.palaiko5g = palaiko;
            return this;
        }
    }
}
