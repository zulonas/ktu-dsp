/**
 * @author Kasparas Zulonas KTU IFIN-8/3
 */
package Lab3zulonas;

import laborai.gui.MyException;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Stream;

public class TelGamyba {

    private static Telefonas [] telefonai;
    private static int pradinisIndeksas = 0, galinisIndeksas = 0;
    private static boolean arPradzia = true;

    public static Telefonas[] generuoti(int kiekis) {
        telefonai = new Telefonas[kiekis];
        for (int i = 0; i < kiekis; i++) {
            telefonai[i] = new Telefonas.Builder().buildRandom();
        }
        return telefonai;
    }

    public static Telefonas[] generuotiIrIsmaisyti(int aibesDydis,
            double isbarstymoKoeficientas) throws MyException {
        return generuotiIrIsmaisyti(aibesDydis, aibesDydis,
                isbarstymoKoeficientas);
    }

    /**
     *
     * @param aibesDydis
     * @param aibesImtis
     * @param isbarstymoKoeficientas
     * @return Gražinamas aibesImtis ilgio masyvas
     * @throws MyException
     */
    public static Telefonas[] generuotiIrIsmaisyti(int aibesDydis,
            int aibesImtis, double isbarstymoKoeficientas) throws MyException {
        telefonai = generuoti(aibesDydis);
        return ismaisyti(telefonai, aibesImtis, isbarstymoKoeficientas);
    }

    // Galima paduoti masyvą išmaišymui iš išorės
    public static Telefonas[] ismaisyti(Telefonas[] telBaze,
            int kiekis, double isbarstKoef) throws MyException {
        if (telBaze == null) {
            throw new IllegalArgumentException("telBaze yra null");
        }
        if (kiekis <= 0) {
            throw new MyException(String.valueOf(kiekis), 1);
        }
        if (telBaze.length < kiekis) {
            throw new MyException(telBaze.length + " >= " + kiekis, 2);
        }
        if ((isbarstKoef < 0) || (isbarstKoef > 1)) {
            throw new MyException(String.valueOf(isbarstKoef), 3);
        }

        int likusiuKiekis = telBaze.length - kiekis;
        int pradziosIndeksas = (int) (likusiuKiekis * isbarstKoef / 2);
        
        Telefonas[] pradineTelefonuImtis = Arrays.copyOfRange(telBaze,
                pradziosIndeksas, pradziosIndeksas + kiekis);
        Telefonas[] likusiTelefonuImtis = Stream
                .concat(Arrays.stream(Arrays.copyOfRange(telBaze, 0,
                        pradziosIndeksas)),
                        Arrays.stream(Arrays.copyOfRange(telBaze,
                                pradziosIndeksas + kiekis, telBaze.length)))
                .toArray(Telefonas[]::new);

        Collections.shuffle(Arrays.asList(pradineTelefonuImtis)
                .subList(0, (int) (pradineTelefonuImtis.length * isbarstKoef)));
        Collections.shuffle(Arrays.asList(likusiTelefonuImtis)
                .subList(0, (int) (likusiTelefonuImtis.length * isbarstKoef)));

        TelGamyba.pradinisIndeksas = 0;
        galinisIndeksas = likusiTelefonuImtis.length - 1;
        TelGamyba.telefonai = likusiTelefonuImtis;
        return pradineTelefonuImtis;
    }

    public static Telefonas gautiIsBazes() throws MyException {
        if ((galinisIndeksas - pradinisIndeksas) < 0) {
            throw new MyException(
                    String.valueOf(galinisIndeksas - pradinisIndeksas), 4);
        }
        //Vieną kartą Telefonas imamas iš masyvo pradžios, kitą kartą - iš galo.     
        arPradzia = !arPradzia;
        return telefonai[arPradzia ? pradinisIndeksas++ : galinisIndeksas--];
    }
}
