/**
 * @author Kasparas Zulonas KTU IFIN-8/3
 */
package Lab3zulonas;

import laborai.studijosktu.Ks;
import laborai.studijosktu.AvlSetKTUx;
import laborai.studijosktu.SortedSetADTx;
import laborai.studijosktu.SetADT;
import laborai.studijosktu.BstSetKTUx;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.Locale;

/*
 * Aibės testavimas be Swing'o
 *
 */
public class TelTestai {

    static Telefonas[] telBaze;
    static SortedSetADTx<Telefonas> aSerija = new BstSetKTUx(new Telefonas());

    public static void main(String[] args) throws CloneNotSupportedException {
        Locale.setDefault(Locale.US); // Suvienodiname skaičių formatus
        aibėsTestas();
    }

    static SortedSetADTx generuotiAibe(int kiekis, int generN) {
        telBaze = new Telefonas[generN];
        for (int i = 0; i < generN; i++) {
            telBaze[i] = new Telefonas.Builder().buildRandom();
        }
        Collections.shuffle(Arrays.asList(telBaze));
        aSerija.clear();
        for (int i = 0; i < kiekis; i++) {
            aSerija.add(telBaze[i]);
        }
        return aSerija;
    }

    public static void aibėsTestas() throws CloneNotSupportedException {
        System.out.printf("--------- Elementų aibės formavimas ------------\n");
        Telefonas a1 = new Telefonas.Builder().buildRandom();
        Telefonas a2 = new Telefonas.Builder().buildRandom();
        Telefonas a3 = new Telefonas.Builder().buildRandom();
        Telefonas a4 = new Telefonas.Builder().buildRandom();
        Telefonas a5 = new Telefonas.Builder().buildRandom();
        Telefonas a6 = new Telefonas.Builder().buildRandom();
        Telefonas a7 = new Telefonas.Builder().buildRandom();
        Telefonas a8 = new Telefonas("Samsung s10 2019 128 950.99 true");
        Telefonas a9 = new Telefonas("Google pixel4 2019 64 1100 true");
        Telefonas a10 = new Telefonas.Builder().buildRandom();
        Telefonas a11 = new Telefonas.Builder().buildRandom();

        Telefonas[] telMasyvas = {a1, a3, a6, a4, a5, a2};

        Ks.oun("Tel Aibė:");
        SortedSetADTx<Telefonas> telAibe = new BstSetKTUx(new Telefonas());

        for (Telefonas a : telMasyvas) {
            telAibe.add(a);
            Ks.oun("Aibė papildoma: " + a + ". Jos dydis: " + telAibe.size());
        }
        Ks.oun("");
        System.out.println(telAibe.toVisualizedString(""));
        
        System.out.printf("\n");
        System.out.printf("--------- Kopijuotos aibės "
                + "sudarymas/elementų įterpimas -----------\n");

        SortedSetADTx<Telefonas> telAibeKopija
                = (SortedSetADTx<Telefonas>) telAibe.clone();

        telAibeKopija.add(a7);
        telAibeKopija.add(a8);
        telAibeKopija.add(a9);
        
        System.out.println("\nPridedami nauji elementai:");
        for (Telefonas a : new Telefonas[]{a7, a8, a9}) {
            Ks.oun(a);
        }
        
        System.out.println("");
        Ks.oun("Papildyta telaibės kopija:");
        System.out.println(telAibeKopija.toVisualizedString(""));
        
        int treeHeight = telAibeKopija.getHeight();                                          
        System.out.println("Medžio auktis - " + treeHeight);
        System.out.println("Lapų akaičius - " + telAibeKopija.getLeafCount());
        
        
        System.out.printf("\n");
        System.out.printf("--------- Priklausomumo aibei tikrinimas"
                + " -----------\n");

        Ks.oun("Ar elementai egzistuoja aibėje?");
        Ks.oun(a2 + ": " + telAibe.contains(a2));
        Ks.oun(a3 + ": " + telAibe.contains(a3));
        Ks.oun(a4 + ": " + telAibe.contains(a4));
        Ks.oun(a10 + ": " + telAibe.contains(a10));
        Ks.oun(a11 + ": " + telAibe.contains(a11));
        Ks.oun("");
        
        System.out.printf("\n");
        System.out.printf("--------- Elementų šalinimas iš kopijuotos aibės "
                + "-----------\n");

        Ks.oun("Elementų šalinimas iš kopijos. Aibės dydis prieš šalinimą:  " +
                telAibeKopija.size());
        for (Telefonas a : new Telefonas[]{a1, a2, a3, a4, a6, a7, a8, a9}) {
            telAibeKopija.remove(a);
            Ks.oun("Iš telaibės kopijos pašalinama: " + a + ". Jos dydis: " +
                    telAibeKopija.size());
        }
        Ks.oun("");
        
        
        System.out.printf("--------- Headset'as medyje "
        + "-----------\n"); 
        
        System.out.printf(telAibe.toVisualizedString(""));
        
        System.out.println("\nPasirinktas elementas");
        Ks.oun(a6);
        System.out.println("\n");
        
        SetADT<Telefonas> newSet = telAibe.headSet(a6);
        
        for (Telefonas a : newSet) {
            Ks.oun(a);
        }
 
        System.out.printf("--------- Kita "
        + "-----------\n");

        
        
        Ks.oun("Telefonų aibė AVL-medyje:");
        SortedSetADTx<Telefonas> telAibe3 = new AvlSetKTUx(new Telefonas());
        for (Telefonas a : telMasyvas) {
            telAibe3.add(a);
        }
        Ks.ounn(telAibe3.toVisualizedString(""));

        Ks.oun("Telefonų aibė su iteratoriumi:");
        Ks.oun("");
        for (Telefonas a : telAibe3) {
            Ks.oun(a);
        }

        Ks.oun("");
        Ks.oun("Telefonų aibė su atvirkštiniu iteratoriumi:");
        Ks.oun("");
        Iterator iter = telAibe3.descendingIterator();
        while (iter.hasNext()) {
            Ks.oun(iter.next());
        }

        Ks.oun("");
        Ks.oun("Telefonų aibės toString() metodas:");
        Ks.ounn(telAibe3);
    }
}
