/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DynamicArray;

import java.util.Arrays;

/**
 * DynamicArray class
 * @author kasparas
 * @param <X>
 */
public class DynamicArray<X> implements DArrayADT<X>{
    private X data[];
    private int size;
    static final int DEF_CAPACITY = 8;

    public DynamicArray() {
        data = (X[]) new Object[DEF_CAPACITY];
        size = 0;
    }

    public DynamicArray(int capacity){
        if (capacity <= 0)
            throw new IllegalArgumentException("Invalid Capacity: " +
                                               capacity);

        data = (X[]) new Object[capacity];
        size = 0;
    }

    @Override
    public boolean contains(X object) {
        if (object == null)
            throw new IllegalArgumentException("Null object detected");

        for (int i = 0; i < size; i++)
            if (data[i].equals(object))
                return true;

        return false;
    }

    @Override
    public X get(int index) {
        if (index >= size || index < 0)
            throw new IndexOutOfBoundsException("Out of Bounds");

        return data[index];
    }

    @Override
    public void set(int index, X object) {
        if (index >= size || index < 0)
            throw new IndexOutOfBoundsException("Out of Bounds");

        data[index] = object;
    }

    @Override
    public void add(X object) {
        if (object == null)
            throw new IllegalArgumentException("Null object detected");

        if (size == data.length)
            reallocArray();

        data[size++] = object;
    }

    @Override
    public void remove(int index) {
        if (index >= size || index < 0)
            throw new IndexOutOfBoundsException("Invalid index");

        for (int i = index; i < size - 1; i++)
            data[i] = data[i+1];

        data[size - 1] = null;
        size--;
    }

    @Override
    public int getCapacity() {
        return data.length;
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return (size == 0);
    }

    public void reallocArray() {
        int newSize = (data.length + DEF_CAPACITY);
        data = Arrays.copyOf(data, newSize);
    }

    @Override
    public void clear() {
        for (int i = 0; i < size; i++)
            data[i + 1] = null;

        size = 0;
    }

    @Override
    public void insert(int index, X object) {
        if (index >= size || index < 0)
            throw new IndexOutOfBoundsException("Out of Bounds");

        if (size == data.length)
            reallocArray();

        if (index == size) {
            data[size++] = object;
            return;
        }

        for (int i = size - 1; i >= index; i--)
            data[i + 1] = data[i];
        data[index] = object;
        size++;
    }

    @Override
    public String toString() {
        String out = "";
        int eil = 0;

        if (size == 0)
            return String.format("Empty array");

        for (int i = 0; i < size; i++)
            out += String.format("%3d: %s \n", eil++, data[i].toString());

        return out;
    }
}
