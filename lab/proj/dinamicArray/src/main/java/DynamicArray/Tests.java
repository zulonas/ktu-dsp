/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DynamicArray;

import java.util.Locale;

/**
 *
 * @author kasparas
 */
public class Tests {

    public static void runTest() {

        DynamicArray<Telefonas> test = new DynamicArray<>();

//      Add, toString
        System.out.println("--- Create dynamic array and add objects ---\n");

        for (int x = 0; x < 8; x++) {
            Telefonas temp = new Telefonas.Builder().buildRandom();
            test.add(temp);
        }
        System.out.println(test);

//      Get
        System.out.println("--- Get specific element ---\n");

        System.out.println("  2 element: " + test.get(2));
        System.out.println("  3 element: " + test.get(3));

//      Insert, getSize, getCapacity
        System.out.println("\n--- Insert element ---\n");

        Telefonas temp_tel = new Telefonas.Builder().buildRandom();
        System.out.println("New element: " + temp_tel + "\n");
        System.out.println("Size before: " + test.getSize());
        System.out.println("Capacity before: " + test.getCapacity());
        test.insert(1, temp_tel);
        System.out.println("\n" + test);
        System.out.println("Size after: " + test.getSize());
        System.out.println("Capacity after: " + test.getCapacity());

//      Contains
        System.out.println("\n--- Check if array contains added element  ---\n");

        System.out.println(temp_tel + " in the array: "
                + test.contains(temp_tel));
        temp_tel = new Telefonas.Builder().buildRandom();
        System.out.println(temp_tel + " in the array: "
                + test.contains(temp_tel));

//      Set
        System.out.println("\n--- Change value of the first element  ---\n");

        System.out.println("Before :\n" + test);
        test.set(0, temp_tel);
        System.out.println("\nAfter: \n" + test);


//      Remove
        System.out.println("\n--- Remove first 3 elements  ---\n");
        for (int x = 0; x < 3; x++)
            test.remove(0);

        System.out.println("After: \n" + test);


//      Clear
        System.out.println("--- Clear the array ---\n");
        test.clear();

//      isEmpty
        System.out.println("--- Check if empty ---\n");
        System.out.println("Empty: " + test.isEmpty());

    }

    public static void main(String... args){
        Locale.setDefault(Locale.US);
        runTest();
    }
}
