/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DynamicArray;

/**
 * Dynamic array interface
 * @author kasparas
 * @param <X>
 */
public interface DArrayADT<X> {
    /**
     * Add new object to the end of array
     * @param object
     */
    void add(X object);

    /**
     * Add new object at specific location
     * @param index
     * @param object
     */
    void insert(int index, X object);

    /**
     * Get object from specific location
     * @param index
     * @return object
     */
    X get(int index);

    /**
     * Check if element exists in array
     * @param object
     * @return true/false
     */
    boolean contains(X object);

    /**
     * Set value of specific array object
     * @param index
     * @param object
     */
    void set(int index, X object);

    /**
     * Remove value at specific index and shift array
     * @param index
     */
    void remove(int index);

    /**
     * Free array
     */
    void clear();

    /**
     * Get size of array
     * @return size of array
     */
    int getSize();

    /**
     * Get capacity of array
     * @return capacity of array
     */
    int getCapacity();

    /**
     * Check whether array is empty
     * @return true/false
     */
    boolean isEmpty();

    /**
     * Iterate throw array and print its elements
     * @return printable string
     */
    @Override
    String toString();
}
