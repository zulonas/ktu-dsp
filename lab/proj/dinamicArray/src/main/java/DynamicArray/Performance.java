/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DynamicArray;

import java.util.LinkedList;
import java.util.Locale;
import java.util.Vector;

/**
 * Peformance class to test overall DynamicArray performance.
 * @author kasparas
 */
public class Performance {
    
    public static void Comparison() {
        long t1, t2;
        int total = 1000;
        DynamicArray darray = new DynamicArray<>();
        LinkedList<Integer> llist = new LinkedList<>();
        Vector<Integer> varray = new Vector<Integer>();

        
        System.out.println("Number of elements: " + total + "\n");
        
        t1 = System.nanoTime();
        for (int i = 0; i < total; i++)
            darray.add(i);
        t2 = System.nanoTime();
        System.out.printf("DynamicArray.add():");
        System.out.printf("\t\t%1.9f", (t2 - t1) * 1e-9);
        System.out.println();
        
        t1 = System.nanoTime();
        for(int i = 0; i < total; i++)
            llist.add(i);
        t2 = System.nanoTime();
        System.out.printf("LinkedList.add():");
        System.out.printf("\t\t%1.9f", (t2 - t1) * 1e-9);
        System.out.println();
        
        t1 = System.nanoTime();
        for(int i = 0; i < total; i++)
            varray.add(i);
        t2 = System.nanoTime();
        System.out.printf("Vector.add():");
        System.out.printf("\t\t\t%1.9f", (t2 - t1) * 1e-9);
        System.out.println();
        
        System.out.println("===============================================\n");
        
        
        t1 = System.nanoTime();
        for (int i = 0; i < total; i++)
            darray.remove(0);
        t2 = System.nanoTime();
        System.out.printf("DynamicArray.remove():");
        System.out.printf("\t\t%1.9f", (t2 - t1) * 1e-9);
        System.out.println();
        
        t1 = System.nanoTime();
        for (int i = 0; i < total; i++)
            llist.remove(0);
        t2 = System.nanoTime();
        System.out.printf("LinkedList.remove():");
        System.out.printf("\t\t%1.9f", (t2 - t1) * 1e-9);
        System.out.println();
        
        t1 = System.nanoTime();
        for(int i = 0; i < total; i++)
            varray.add(0);
        t2 = System.nanoTime();
        System.out.printf("Vector.remove():");
        System.out.printf("\t\t%1.9f", (t2 - t1) * 1e-9);
        System.out.println();
    }
    
    public static void main(String... args){
        Locale.setDefault(Locale.US);
        Comparison();
    }
}
