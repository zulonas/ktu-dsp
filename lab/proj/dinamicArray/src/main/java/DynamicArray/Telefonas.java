/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DynamicArray;

import java.util.Random;

/**
 *
 * @author kasparas
 */
public class Telefonas {
    private String gamintojas;
    private String modelis;
    private int isleidimoMetai;
    private double kaina;


    public Telefonas() {
        gamintojas = "";
        modelis = "";
        isleidimoMetai = -1;
        kaina = -1;
    }

    public Telefonas(String gamintojas, String modelis, int metai,
            double kaina) {
        this.gamintojas = gamintojas;
        this.modelis = modelis;
        this.isleidimoMetai = metai;
        this.kaina = kaina;
    }

    private Telefonas(Telefonas.Builder builder) {
        this.gamintojas = builder.gamintojas;
        this.modelis = builder.modelis;
        this.isleidimoMetai = builder.isleidimoMetai;
        this.kaina = builder.kaina;
    }

    public String getGamintoja() {
        return gamintojas;
    }

    public String getModelis() {
        return modelis;
    }

    public int getIsleidimoMetai() {
        return isleidimoMetai;
    }

    public double getKaina() {
        return kaina;
    }

    @Override
    public String toString() {
        return String.format("%-8s \t %-8s %4d %8.1f",
                gamintojas, modelis, isleidimoMetai, kaina);
    }


    public static class Builder {
        private final static Random RANDOM = new Random(498415461);
        private final static String[][] MODELIAI = {
            {"Iphone", "6", "6P", "7", "7P", "X", "XS", "XR"},
            {"Huaway", "P10", "P9", "P20", "P30"},
            {"Samsung", "S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8",
                "S9", "S10"},
            {"Google", "6p", "pixel1", "pixel2", "pixel3", "pixel4"},
        };

        private String gamintojas;
        private String modelis;
        private int isleidimoMetai;
        private double kaina;

        public Telefonas build() {
            return new Telefonas(this);
        }

        public Telefonas buildRandom() {
            int ma = RANDOM.nextInt(MODELIAI.length);
            int mo = RANDOM.nextInt(MODELIAI[ma].length - 1) + 1;
            return new Telefonas(MODELIAI[ma][0],
                    MODELIAI[ma][mo],
                    2007 + RANDOM.nextInt(10), // metai tarp 2007 ir 2017
                    100 + RANDOM.nextDouble() * 2000); //kaina);
        }

        public Builder gamintojas(String gamintojas) {
            this.gamintojas = gamintojas;
            return this;
        }

        public Builder isleidMetai(int isleidMetai) {
            this.isleidimoMetai = isleidMetai;
            return this;
        }

        public Builder kaina(double kaina) {
            this.kaina = kaina;
            return this;
        }

        public Builder modelis(String modelis) {
            this.modelis = modelis;
            return this;
        }
    }

}
