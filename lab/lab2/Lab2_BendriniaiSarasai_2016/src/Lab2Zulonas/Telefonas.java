/**
 * @author Kasparas Zulonas KTU IFIN-8/3
 */
package Lab2Zulonas;

import java.io.InputStream;
import java.io.PrintStream;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.InputMismatchException;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Scanner;
import studijosKTU.*;

public class Telefonas implements KTUable<Telefonas> {
    final static private int priimtinųMetųRiba = 2014;
    final static private int esamiMetai = LocalDate.now().getYear();
    private String gamintojas;
    private String modelis;
    private int isleidimoMetai;
    private int talpa;
    private double kaina;
    private boolean palaiko5G;

    public Telefonas() {
    }

    public Telefonas(String gamintojas, String modelis,
                    int metai, int talpa, double kaina, boolean palaiko5G) {
        this.gamintojas = gamintojas;
        this.modelis = modelis;
        this.isleidimoMetai = metai;
        this.talpa = talpa;
        this.kaina = kaina;
        this.palaiko5G = palaiko5G;
    }

    public Telefonas(String dataString) {
        this.parse(dataString);
    }
    
    public String getGamintojas() {
        return gamintojas;
    }

    public String getModelis() {
        return modelis;
    }

    public int getIsleidimoMetai() {
        return isleidimoMetai;
    }

    public int getTalpa() {
        return talpa;
    }

    public double getKaina() {
        return kaina;
    }

    public void setKaina(double kaina) {
        this.kaina = kaina;
    }
    
    public boolean getPalaiko5g() {
        return palaiko5G;
    }

    /**
     * Sukuria naują objektą iš eilutės
     * @param dataString eilutė objekto sukūrimui
     * @return Automobilio klasės objektą
     */
    @Override
    public Telefonas create(String dataString) {
        return new Telefonas(dataString);
    }
    
    /**
     * Suformuoja objektą iš eilutės
     * @param dataString eilutė objektui formuoti
     */
    @Override
    public final void parse(String dataString) {
        try {   
            Scanner ed = new Scanner(dataString); 
            // numatytieji skirtukai: tarpas, tab, eilutės pabaiga
            // Skiriklius galima pakeisti Scanner klasės metodu useDelimitersr 
            // Pavyzdžiui, ed.useDelimiter(", *"); reikštų, kad skiriklis 
            // bus kablelis ir vienas ar daugiau tarpų.
            gamintojas = ed.next();
            modelis = ed.next();
            isleidimoMetai = ed.nextInt();
            talpa = ed.nextInt();
            setKaina(ed.nextDouble());
            palaiko5G = ed.nextBoolean();
        } catch (InputMismatchException e) {
            Ks.ern("Blogas duomenų formatas apie telefoną -> " + dataString);
        } catch (NoSuchElementException e) {
            Ks.ern("Trūksta duomenų apie telefoną -> " + dataString);
        }
    }

    /**
     * Suformuoja objektą iš eilutės
     * @param dataString eilutė objektui formuoti
     */
    public final String parseErr(String dataString) {
        try {   
            Scanner ed = new Scanner(dataString); 
            // numatytieji skirtukai: tarpas, tab, eilutės pabaiga
            // Skiriklius galima pakeisti Scanner klasės metodu useDelimitersr 
            // Pavyzdžiui, ed.useDelimiter(", *"); reikštų, kad skiriklis 
            // bus kablelis ir vienas ar daugiau tarpų.
            gamintojas = ed.next();
            modelis = ed.next();
            isleidimoMetai = ed.nextInt();
            talpa = ed.nextInt();
            setKaina(ed.nextDouble());
            palaiko5G = ed.nextBoolean();
        } catch (InputMismatchException e) {
            return String.format("Blogas duomenų formatas apie telefoną -> " + dataString);
        } catch (NoSuchElementException e) {
            return String.format("Trūksta duomenų apie telefoną -> " + dataString);
        }
        
        return "";
    }

    /**
     * Patikrina objekto reikšmes pagal norimmas taisykles
     * @return tuščią eilutę arba eilutę-klaidos tipą
     */
    @Override
    public String validate() {
        String klaidosTipas = "";
        if (isleidimoMetai < priimtinųMetųRiba || isleidimoMetai > esamiMetai) {
            klaidosTipas = "Netinkami gamybos metai, turi būti [" 
                    + priimtinųMetųRiba + ":" + esamiMetai + "]";
        }
        if (kaina > 1000) {
            klaidosTipas += " Kaina per brangi, turėtų būti: " + (kaina - 1000)
                    + "€ pigesnė";
        }
        return klaidosTipas;
    }

    /**
     * Objekto reikšmių išvedimas, nurodant išvedime tik objekto vardą
     * @return Išvedimui suformuota eilutė
     */
    @Override
    public String toString() {  // surenkama visa reikalinga informacija
        return String.format("%-8s \t %-8s %4d %7d %8.1f %8d %s",
                gamintojas, modelis, isleidimoMetai, talpa, kaina,
                (palaiko5G == true)? 1: 0, validate());
    }


    /**
     * Interfeiso Comparable metodas, naudojamas objektų rikiavimui
     * @param a objektas su kuriuo lyginama
     * @return 0 | 1 | -1
     */
    @Override
    public int compareTo(Telefonas a) {
        // lyginame pagal svarbiausią požymį - kainą
        double kainaKita = a.getKaina();
        return Double.compare(kainaKita, kaina);
    }

    // Rikiavimo pagal modeli komparatorius
    public final static Comparator<Telefonas> pagalGamintoja
            = (Telefonas a1, Telefonas a2) -> {
        int cmp = a1.getGamintojas().compareTo(a2.getGamintojas());
        return cmp;
    };

    /**
     * Rikiavimo pagal kainą komparatorius
     *	(geriau panaudoti Double klasės metodą compare)
     */
    public final static Comparator<Telefonas> pagalKainą
            = (Telefonas o1, Telefonas o2) -> {
                double k1 = o1.getKaina();
                double k2 = o2.getKaina();
                // didėjanti tvarka, pradedant nuo mažiausios
                if (k1 < k2) {
                    return -1;
                }
                if (k1 > k2) {
                    return 1;
                }
                return 0;
    };

    /**
     * Rikiavimo pagal metus ir kainą komparatorius
     */
    public final static Comparator pagalMetusKainą =
            (Comparator) (Object o1, Object o2) -> {
        Telefonas a1 = (Telefonas) o1;
        Telefonas a2 = (Telefonas) o2;
        // metai mažėjančia tvarka, esant vienodiems metams, lyginama kaina
        if(a1.getIsleidimoMetai() < a2.getIsleidimoMetai())
            return 1;
        if(a1.getIsleidimoMetai() > a2.getIsleidimoMetai())
            return -1;
        if(a1.getKaina() < a2.getKaina())
            return 1;
        if(a1.getKaina() > a2.getKaina())
            return -1;
        return 0;
    } // sarankiškai priderinkite prie generic interfeiso ir Lambda funkcijų
    ;

    /**
     * Main metodas
     * @param args 
     */
    public static void main(String... args) {
        // suvienodiname skaičių formatus pagal LT lokalę (10-ainis kablelis)
        ListKTU<Telefonas> telefonai = new ListKTU<>();
        Locale.setDefault(new Locale("LT"));
        Telefonas a1 = new Telefonas("Apple", "7s", 2017, 32, 500, false);
        Telefonas a2 = new Telefonas("Huaway", "p10", 2018, 32, 320 ,false);
        Telefonas a3 = new Telefonas();
        Telefonas a4 = new Telefonas();
        Telefonas a5 = new Telefonas();
        Telefonas a6 = new Telefonas();
        Telefonas a7 = new Telefonas();
        a3.parse("Samsung s10 2018 64 950,0 true");
        a4.parse("Google pixel4 2019 32 950 true");
        a5.parse("Samsung flex 2019 64 1350 false");
        a6.parse("Za flex 2019 64 1350 false");
        a7.parse("Ab flex 2019 64 1350 false");
        telefonai.addFirst(a1);
        telefonai.addFirst(a2);
        telefonai.addFirst(a3);
        telefonai.remove(0);
        telefonai.sortMinMax(pagalKainą);
        telefonai.printAll();
    }
}
