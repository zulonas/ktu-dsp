/**
 * @author Kasparas Zulonas KTU IFIN-8/3
 */
package Lab2Zulonas;

import java.util.Iterator;
import studijosKTU.*;

public class TelefonuParduotuve {

    public ListKTUx<Telefonas> visiTel = new ListKTUx<>(new Telefonas());
    private static final Telefonas bazinisEgz = new Telefonas();

    // suformuojamas sąrašas telefonų, kurių kaina yra nurodytame intervale
    public ListKTUx<Telefonas> atrinktiPagalKainą(int riba1, int riba2) {
        ListKTUx<Telefonas> vidutiniaiAuto = new ListKTUx(bazinisEgz);
        for (Telefonas a : visiTel) {
            if (a.getKaina() >= riba1 && a.getKaina() <= riba2) {
                vidutiniaiAuto.addLast(a);
            }
        }
        return vidutiniaiAuto;
    }

    // suformuojamas sąrašas telefonų, turinčių max kainą 
    //	(tą pačią max kainą gali turėti ir keli telefonai)
    public ListKTUx<Telefonas> maksimaliosKainosTel() {
        ListKTUx<Telefonas> brangiausiAuto = new ListKTUx(bazinisEgz);
        
        // formuojamas sąrašas su maksimalia reikšme vienos peržiūros metu
        double maxKaina = 0;
        for (Telefonas a : visiTel) {
            double kaina = a.getKaina();
            if (kaina >= maxKaina) {
                if (kaina > maxKaina) {
                    brangiausiAuto.clear();
                    maxKaina = kaina;
                }
                brangiausiAuto.addLast(a);
            }
        }
        
        return brangiausiAuto;
    }

    // suformuojams sąrašas telefonų, kurių modelio kodas atitinka nurodytą
    // Ciklas su iteratoriumi
    public ListKTUx<Telefonas> atrinktiMarkęModelį(String modelioKodas) {
        ListKTUx<Telefonas> firminiaiAuto = new ListKTUx(bazinisEgz);
        
        for (Iterator<Telefonas> it = visiTel.iterator(); it.hasNext();) {
            Telefonas a = it.next();
            String pilnasModelis = a.getGamintojas() + " " + a.getModelis();
            if (pilnasModelis.startsWith(modelioKodas))
                firminiaiAuto.addLast(a);
        }
        
        return firminiaiAuto;
    }
}
