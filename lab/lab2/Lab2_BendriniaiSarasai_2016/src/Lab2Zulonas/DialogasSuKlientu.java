/**
 * @author Kasparas Zulonas KTU IFIN-8/3
 */
package Lab2Zulonas;

import java.util.Locale;
import studijosKTU.*;

public class DialogasSuKlientu {

    TelefonuParduotuve aTurgus = new TelefonuParduotuve();

    void bendravimasSuKlientu() {
        ListKTUx<Telefonas> atranka = null;
        int varNr;  // skaičiavimo varijantas pasirenkamas nurodant jo numerį
        String dialogMeniu = "Pasirinkimas: "
                + "1-skaityti iš failo; 2-papildyti sąrašą; 3-naujų atranka;\n    "
                + "4-atranka pagal kainą; 5-brangiausi auto;\n    "
                + "0-baigti skaičiavimus > ";
        while ((varNr = Ks.giveInt(dialogMeniu, 0, 6)) != 0) {
            switch (varNr) {
                case 1:
                    aTurgus.visiTel.load(Ks.giveFileName());
                    aTurgus.visiTel.println("Visų automobilių sąrašas");
                    break;
                case 2:
                    String autoDuomenys = Ks.giveString("Nurodykite auto markę, "
                            + "modelį, gamybos metus, ridą ir kainą\n ");
                    Telefonas a = new Telefonas();
                    a.parse(autoDuomenys);
                    String klaidosPožymis = a.validate();
                    if (klaidosPožymis.isEmpty()) // dedame tik su gerais duomenimis
                    {
                        aTurgus.visiTel.addLast(a);
                    } else {
                        Ks.oun("!!! Automobilis į sąrašą nepriimtas " + klaidosPožymis);
                    }
                    break;
                default:
                    // toliau vykdomos atskiri atrankos metodai
                    switch (varNr) {
                        case 3:
                            int nR = Ks.giveInt("Nurodykite naujų auto metų ribą: ");
                            Ks.oun("Naujų automobilių atranka dar nerealizuota.");
                            break;
                        case 4:
                            int r1 = Ks.giveInt("Nurodykite apatinę kainos ribą: ");
                            int r2 = Ks.giveInt("Nurodykite viršutinę kainos ribą: ");
                            atranka = aTurgus.atrinktiPagalKainą(r1, r2);
                            break;
                        case 5:
                            atranka = aTurgus.maksimaliosKainosTel();
                            break;
                    }
                    if (atranka != null) {
                        atranka.println("Štai atrinktų automobilių sąrašas");
                        atranka.save(Ks.giveString("Kur saugoti atrinktus auto (jei ne-tai ENTER) ? "));
                    }
                    break;
            }
        }
    }

    public static void main(String[] args) {
        // suvienodiname skaičių formatus pagal LT lokalę (10-ainis kablelis)
        Locale.setDefault(new Locale("LT"));
        new DialogasSuKlientu().bendravimasSuKlientu();
    }
}
