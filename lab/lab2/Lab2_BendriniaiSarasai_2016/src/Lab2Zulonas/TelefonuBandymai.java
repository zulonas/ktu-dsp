/**
 * @author Kasparas Zulonas KTU IFIN-8/3
 */
package Lab2Zulonas;

import java.util.Locale;
import studijosKTU.*;

public class TelefonuBandymai {
    ListKTUx<Telefonas> bandomieji = new ListKTUx<>(new Telefonas());
    ListKTU<Telefonas> kopija;

    void metodoParinkimas() {
        //tikrintiAtskirusAuto();
        //formuotiAutoSąrašą();
        //peržiūrėtiSąrašą();
        //papildytiSąrašą();
        //patikrintiTurgausApskaitą();
        patikrintiRikiavimą();
    }

    void tikrintiAtskirusAuto() {
        Telefonas a1 = new Telefonas("Apple", "6p", 2015, 64, 1500, true);
        Telefonas a2 = new Telefonas("Samsung", "s10", 2017, 64, 900, false);
        Telefonas a3 = new Telefonas("Google", "pixel", 2019, 100, 700, true);
        Telefonas a4 = new Telefonas();
        Telefonas a5 = new Telefonas();
        Telefonas a6 = new Telefonas();
        a4.parse("Google pixel 2001 128 720 true");
        a5.parse("Sony asd 1998 64 950 false");
        a6.parse("Oneplus Civic 2015 128 300 true");

        Ks.oun(a1);
        Ks.oun(a2);
        Ks.oun(a3);
        Ks.oun("Pirmų 3 telefonų kainos vidurkis= "
                + (a1.getTalpa() + a2.getTalpa() + a3.getTalpa()) / 3);
        Ks.oun(a4);
        Ks.oun(a5);
        Ks.oun(a6);
        Ks.oun("Pirmų 3 telefonų kainų suma= "
                + (a4.getKaina() + a5.getKaina() + a6.getKaina()));
    }
    
    void formuotiTelSarasa() {
        Telefonas a1 = new Telefonas("Huaway", "5", 1997, 128, 700, true);
        Telefonas a2 = new Telefonas("Motorola", "a2", 2001, 64, 940, true);
        Telefonas a3 = new Telefonas("Huaway", "7p", 2016, 128, 1280.30, false);
        bandomieji.addLast(a1);
        bandomieji.addLast(a2);
        bandomieji.addLast(a3);
        bandomieji.println("PIRMI 3 TELEFONAI");

        // Kitas sąrašas - pirmų trijų telefonų kopija
        kopija = bandomieji.clone(); 

        bandomieji.add("Google pixel 2001 128 720 true");
        bandomieji.add("Sony asd 1998 64 950 false");
        bandomieji.add("Oneplus Civic 2015 128 300 true");

        bandomieji.println("VISI 6 TELEFONAI");

        Ks.oun("Kopijos elementai");
        for (Telefonas a : kopija) {
            Ks.oun(a);
        }
        
        Ks.oun("Kopijos elementai su nuoroda ::");
        kopija.forEach(System.out::println); // Galima "mandriau" - su nuoroda ::

        Ks.oun("\nPirmų 3 telefonu kainos vidurkis= "
                + (bandomieji.get(0).getKaina() + bandomieji.get(1).getKaina()
                + bandomieji.get(2).getKaina()) / 3);
    }

    void peržiūrėtiSąrašą() {
        int sk = 0;
        for (Telefonas a : bandomieji) {
            if (a.getGamintojas().compareTo("Apple") == 0) {
                sk++;

            }
        }
        Ks.oun("Apple telefonų yra = " + sk);

        // Kopijos testas:
        sk = 0;
        for (Telefonas a : kopija) {
            if (a.getGamintojas().compareTo("Huawei") == 0) {
                sk++;
            }
        }
        Ks.oun("Huawei telefonų yra = " + sk);
    }

    void papildytiSąrašą() {
        for (int i = 0; i < 8; i++) {
            bandomieji.addLast(new Telefonas("Google", "pixel", 2018 - i, i * 64, i * 150, (i % 2 == 0)));
        }
        bandomieji.add("NOkia pixel 2017 128 720 true");
        bandomieji.add("Sony asd 1998 64 950 false");
        bandomieji.println("Testuojamų telefonų sąrašas");
        bandomieji.save("ban.txt");
    }

    void patikrintiTurgausApskaitą() {
        TelefonuParduotuve aTurgus = new TelefonuParduotuve();

        aTurgus.visiTel.load("ban.txt");
        aTurgus.visiTel.println("Bandomasis rinkinys");

        bandomieji = aTurgus.atrinktiPagalKainą(1000, 1500);
        bandomieji.println("Kaina tarp 1000 ir 1500");

        bandomieji = aTurgus.maksimaliosKainosTel();
        bandomieji.println("Patys brangiausi");

        bandomieji = aTurgus.atrinktiMarkęModelį("S");
        bandomieji.println("Turi būti tik Spple ir Sony");

        bandomieji = aTurgus.atrinktiMarkęModelį("Apple");
        bandomieji.println("Turi būti tik Apple");
        Ks.oun("Apple kiekis = " + bandomieji.size());
    }

    void patikrintiRikiavimą() {
        TelefonuParduotuve aps = new TelefonuParduotuve();

        aps.visiTel.load("ban.txt");
        Ks.oun("========");
        aps.visiTel.println("Bandomasis rinkinys");
        
        aps.visiTel.sortBuble(Telefonas.pagalGamintoja);
        aps.visiTel.println("Rūšiavimas pagal Gamintoją");
        
        aps.visiTel.sortBuble(Telefonas.pagalKainą);
        aps.visiTel.println("Rūšiavimas pagal kainą");
        
        
        aps.visiTel.sortBuble(Telefonas.pagalMetusKainą);
        aps.visiTel.println("Rūšiaviamas pagal metus ir kainą");
    }

    /**
     * Metodų testas
     *
     * @param args
     */
    public static void main(String... args) {
        Locale.setDefault(new Locale("LT"));
        new TelefonuBandymai().metodoParinkimas();
    }
}
