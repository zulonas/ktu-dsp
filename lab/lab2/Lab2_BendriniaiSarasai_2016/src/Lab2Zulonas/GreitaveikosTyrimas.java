/**
 * @author Kasparas Zulonas KTU IFIN-8/3
 */
package Lab2Zulonas;

import java.util.Arrays;
import java.util.Collections;
import java.util.Locale;
import java.util.Random;
import studijosKTU.*;

public class GreitaveikosTyrimas {

    Telefonas[] telefonai;
    ListKTU<Telefonas> aSeries = new ListKTU<>();
    Random ag = new Random();  // atsitiktinių generatorius
    int[] tiriamiKiekiai = {2_000, 4_000, 8_000, 16_000};
//    pabandykite, gal Jūsų kompiuteris įveiks šiuos eksperimentus
//    paieškokite ekstremalaus apkrovimo be burbuliuko metodo
//    static int[] tiriamiKiekiai = {10_000, 20_000, 40_000, 80_000};

    void generuotiTelefonus(int kiekis) {
        String[][] am = { // galimų telefonų markių ir jų modelių masyvas
            {"Samsung", "s5", "s6", "s7", "s8min"},
            {"Apple", "6p", "7", "7p", "8", "x"},
            {"Huawai", "p10", "p20"},
            {"Google", "pixel1", "pixel2", "nexus6p"},
            {"Motorola", "asd", "dsa", "qaz", "zaq"},
            {"Microsoft", "lumia", "3310", "n1"}
        };
        
        telefonai = new Telefonas[kiekis];
        ag.setSeed(2016);
        for (int i = 0; i < kiekis; i++) {
            int ma = ag.nextInt(am.length);        // markės indeksas  0..
            int mo = ag.nextInt(am[ma].length - 1) + 1;// modelio indeksas 1..
            telefonai[i] = new Telefonas(
                    am[ma][0],
                    am[ma][mo],
                    2010 + ag.nextInt(10), // metai tarp 2010 ir 2020
                    12 + ag.nextInt(1024), // rida tarp 12 ir 1024
                    100 + ag.nextDouble() * 1500, //kaina tarp 100-1500
                    (ma % 2 == 0)); 
        }
        Collections.shuffle(Arrays.asList(telefonai));
        aSeries.clear();
        for (Telefonas a : telefonai) {
            aSeries.addLast(a);
        }
    }

    void paprastasTyrimas(int elementųKiekis) {
        // Paruošiamoji tyrimo dalis
        long t0 = System.nanoTime();
        generuotiTelefonus(elementųKiekis);
        ListKTU<Telefonas> aSeries2 = aSeries.clone();
        ListKTU<Telefonas> aSeries3 = aSeries.clone();
        ListKTU<Telefonas> aSeries4 = aSeries.clone();
        long t1 = System.nanoTime();
        System.gc();
        System.gc();
        System.gc();
        long t2 = System.nanoTime();

        //  Greitaveikos bandymai ir laiko matavimai
        aSeries.sortJava();
        long t3 = System.nanoTime();
        aSeries2.sortJava(Telefonas.pagalKainą);
        long t4 = System.nanoTime();
        aSeries3.sortBuble();
        long t5 = System.nanoTime();
        aSeries4.sortMinMax(Telefonas.pagalKainą);
        long t6 = System.nanoTime();
        Ks.ouf("%7d %7.4f %7.4f %7.4f %7.4f %7.4f %7.4f \n", elementųKiekis,
                (t1 - t0) / 1e9, (t2 - t1) / 1e9, (t3 - t2) / 1e9,
                (t4 - t3) / 1e9, (t5 - t4) / 1e9, (t6 - t5) / 1e9);
    }
    
    void pmanoTyrimas(int elementųKiekis) {
        // Paruošiamoji tyrimo dalis
        long t0 = System.nanoTime();
        generuotiTelefonus(elementųKiekis);
        ListKTU<Telefonas> aSeries2 = aSeries.clone();
        ListKTU<Telefonas> aSeries3 = aSeries.clone();
        ListKTU<Telefonas> aSeries4 = aSeries.clone();
        long t1 = System.nanoTime();
        System.gc();
        System.gc();
        System.gc();
        long t2 = System.nanoTime();

        //  Greitaveikos bandymai ir laiko matavimai
        aSeries.sortJava();
        long t3 = System.nanoTime();
        aSeries2.sortJava(Telefonas.pagalKainą);
        long t4 = System.nanoTime();
        aSeries3.sortBuble();
        long t5 = System.nanoTime();
        aSeries4.sortMinMax(Telefonas.pagalKainą);
        long t6 = System.nanoTime();
        Ks.ouf("%7d %7.4f %7.4f %7.4f %7.4f %7.4f %7.4f \n", elementųKiekis,
                (t1 - t0) / 1e9, (t2 - t1) / 1e9, (t3 - t2) / 1e9,
                (t4 - t3) / 1e9, (t5 - t4) / 1e9, (t6 - t5) / 1e9);
    }

    void tyrimoPasirinkimas() {
        long memTotal = Runtime.getRuntime().totalMemory();
        Ks.oun("memTotal= " + memTotal);
        // Pasižiūrime kaip generuoja telefonus (20) vienetų)
        generuotiTelefonus(16_000);
        for (Telefonas a : aSeries) {
            Ks.oun(a);
        }
        Ks.oun("1 - Pasiruošimas tyrimui - duomenų generavimas");
        Ks.oun("2 - Pasiruošimas tyrimui - šiukšlių surinkimas");
        Ks.oun("3 - Rūšiavimas sisteminiu greitu būdu be Comparator");
        Ks.oun("4 - Rūšiavimas sisteminiu greitu būdu su Comparator");
        Ks.oun("5 - Rūšiavimas List burbuliuku be Comparator");
        Ks.oun("6 - Rūšiavimas List burbuliuku su Comparator");
        Ks.ouf("%6d %7d %7d %7d %7d %7d %7d \n", 0, 1, 2, 3, 4, 5, 6);
        for (int n : tiriamiKiekiai) {
            paprastasTyrimas(n);
        }
        Ks.oun("Rikiavimo metodų greitaveikos tyrimas baigtas.");
    }

    public static void main(String[] args) {
        // suvienodiname skaičių formatus pagal LT lokalę (10-ainis kablelis)
        Locale.setDefault(new Locale("LT"));
        //new GreitaveikosTyrimas().tyrimoPasirinkimas();
        new GreitaveikosTyrimas().paprastasTyrimas(1_000);
        new GreitaveikosTyrimas().paprastasTyrimas(2_000);
        new GreitaveikosTyrimas().paprastasTyrimas(4_000);
        new GreitaveikosTyrimas().paprastasTyrimas(8_000);
        new GreitaveikosTyrimas().paprastasTyrimas(16_000);
        new GreitaveikosTyrimas().paprastasTyrimas(32_000);
        //new GreitaveikosTyrimas().paprastasTyrimas(64_000);
    }
}
