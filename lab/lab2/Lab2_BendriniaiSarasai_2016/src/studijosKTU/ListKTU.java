/**
 * @author Kasparas Zulonas KTU IFIN-8/3
 */
package studijosKTU;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;

/**
 * Pagrindinių sąrašo operacijų klasė.
 *
 * @param <E> Sąrašo elementų tipas (klasė)
 */
public class ListKTU<E extends Comparable<E>>
        implements ListADT<E>, Iterable<E>, Cloneable {

    private Node<E> first;   // rodyklė į pirmą mazgą
    private Node<E> last;    // rodyklė į paskutinį mazgą
    private Node<E> current; // rodyklė į einamąjį mazgą, naudojama metode getNext
    private int size;        // sąrašo dydis, tuo pačiu elementų skaičius

    /**
     * Metodas add įdeda elementą į sąrašo pabaigą
     *
     * @param e - tai įdedamas elementas (jis negali būti null)
     * @return true, jei operacija atlikta sėkmingai
     */
    @Override
    public boolean addLast(E e) {
        if (e == null) {
            throw new NullPointerException("Naujas objektas sąraše negali būti neapibrėžtas");
        }
            
        if (first == null) {
            first = new Node<>(e, null, null);
            last = first;
        } else {
            Node<E> e1 = new Node(e, null, last);
            last.next = e1;
            last = e1;
        }
        size++;
        return true;
    }
    
    /**
     * Metodas add įdeda elementą į sąrašo pražią
     *
     * @param e - tai įdedamas elementas (jis negali būti null)
     * @return true, jei operacija atlikta sėkmingai
     */
    @Override
    public boolean addFirst(E e) {
        if (e == null) {
            throw new NullPointerException("Naujas objektas sąraše negali būti neapibrėžtas");
        }
        
        if (first == null) {
            first = new Node<>(e, null, null);
            last = first;
        } else {
            Node<E> e1 = new Node(e, first, null);
            first.prev = e1;
            first = e1;
        }
        size++;
        return true;
    }

    /**
     *
     * @return sąrašo dydis (elementų kiekis)
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Patikrina ar sąrašas yra tuščias
     *
     * @return true, jei tuščias
     */
    @Override
    public boolean isEmpty() {
        return first == null;
    }

    /**
     * Išvalo sąrašą
     */
    @Override
    public void clear() {
        size = 0;
        first = null;
        last = null;
        current = null;
    }

    /**
     * Grąžina elementą pagal jo indeksą (pradinis indeksas 0)
     *
     * @param k indeksas
     * @return k-ojo elemento reikšmę; jei k yra blogas, gąžina null
     */
    @Override
    public E get(int k) {
        if (k < 0 || k >= size) {
            return null;
        }
        current = first.findNode(k);
        return current.element;
    }

    /**
     * Pereina prie kitos reikšmės ir ją grąžina
     *
     * @return kita reikšmė
     */
    @Override
    public E getNext() {
        if (current == null) {
            return null;
        }
        current = current.next;
        if (current == null) {
            return null;
        }
        return current.element;
    }
    
    @Override
    public E getPrev() {
        if (current == null) {
            return null;
        }
        current = current.prev;
        if (current == null) {
            return null;
        }
        return current.element;
    }

    /**
     * Sukuria sąrašo kopiją.
     *
     * @return sąrašo kopiją
     */
    @Override
    public ListKTU<E> clone() {
        ListKTU<E> cl = null;
        
        try {
            cl = (ListKTU<E>) super.clone();
        } catch (CloneNotSupportedException e) {
            Ks.ern("Blogai veikia ListKTU klasės protėvio metodas clone()");
            System.exit(1);
        }
        if (first == null) {
            return cl;
        }
        
        cl.first = new Node<>(this.first.element, null, null);
        Node<E> e2 = cl.first;
        for (Node<E> e1 = first.next; e1 != null; e1 = e1.next) {
            e2.next = new Node<>(e1.element, null, e2);
            e2 = e2.next;
        }
        cl.last = e2;
        cl.size = this.size;
        
        return cl;
    }

    /**
     * Formuojamas Object masyvas (E tipo masyvo negalima sukurti), kur surašomi
     * sąrašo elementai
     *
     * @return sąrašo elementų masyvas
     */
    @Override
    public Object[] toArray() {
        Object[] a = new Object[size];
        int i = 0;
        for (Node<E> e1 = first; e1 != null; e1 = e1.next) {
            a[i++] = e1.element;
        }
        return a;
    }

    /**
     * Masyvo rikiavimas Arrays klasės metodu sort
     */
    public void sortJava() {
        Object[] a = this.toArray();
        Arrays.sort(a);
        int i = 0;
        for (Node<E> e1 = first; e1 != null; e1 = e1.next) {
            e1.element = (E) a[i++];
        }
    }

    /**
     * Rikiavimas Arrays klasės metodu sort pagal komparatorių
     *
     * @param c komparatorius
     */
    public void sortJava(Comparator<E> c) {
        Object[] a = this.toArray();
        Arrays.sort(a, (Comparator) c);
        int i = 0;
        for (Node<E> e1 = first; e1 != null; e1 = e1.next) {
            e1.element = (E) a[i++];
        }
    }

    /**
     * Sąrašo rikiavimas burbuliuko metodu
     */
    public void sortBuble() {
        if (first == null) {
            return;
        }
        for (;;) {
            boolean jauGerai = true;
            Node<E> e1 = first;
            for (Node<E> e2 = first.next; e2 != null; e2 = e2.next) {
                if (e1.element.compareTo(e2.element) > 0) {
                    E t = e1.element;
                    e1.element = e2.element;
                    e2.element = t;
                    jauGerai = false;
                }
                e1 = e2;
            }
            if (jauGerai) {
                return;
            }
        }
    }

    /**
     * Sąrašo rikiavimas burbuliuko metodu pagal komparatorių
     *
     * @param c komparatorius
     */
    public void sortBuble(Comparator<E> c) {
        if (first == null) {
            return;
        }
        for (;;) {
            boolean jauGerai = true;
            Node<E> e1 = first;
            for (Node<E> e2 = first.next; e2 != null; e2 = e2.next) {
                if (c.compare(e1.element, e2.element) > 0) {
                    E t = e1.element;
                    e1.element = e2.element;
                    e2.element = t;
                    jauGerai = false;
                }
                e1 = e2;
            }
            if (jauGerai) {
                return;
            }
        }
    }

    /**
     * Sąrašo rikiavimas burbuliuko metodu pagal komparatorių
     *
     * @param c komparatorius
     */
    public void sortMinMax(Comparator<E> c) {
        if (first == null) {
            throw new NullPointerException("Sarašas tuščias");
        }

        for (Node<E> e1 = first; e1 != null; e1 = e1.next) {
            for (Node<E> e2 = e1.next; e2 != null; e2 = e2.next) {
                if (c.compare(e1.element, e2.element) > 0) {
                    E t = e1.element;
                    e1.element = e2.element;
                    e2.element = t;
                }
            }
        }
    }

    /**
     * Sukuria iteratoriaus objektą sąrašo elementų peržiūrai
     *
     * @return iteratoriaus objektą
     */
    @Override
    public Iterator<E> iterator() {
        return new ListIteratorKTU();
    }

    /**
     * Iteratoriaus metodų klasė
     */
    private class ListIteratorKTU implements Iterator<E> {

        private Node<E> iterPosition;

        ListIteratorKTU() {
            iterPosition = first;
        }

        @Override
        public boolean hasNext() {
            return iterPosition != null;
        }

        @Override
        public E next() {
            E d = iterPosition.element;
            iterPosition = iterPosition.next;
            return d;
        }
    }

    /**
     * Vidinė mazgo klasė
     *
     * @param <E> mazgo duomenų tipas
     */
    private static class Node<E> {

        private E element;    // ji nematoma už klasės ListKTU ribų
        private Node<E> next;
        private Node<E> prev; 

        /**
         * Naujo mazgo(node) konstruktorius
         * @param e naujas obj 
         * @param next sekantis(next) mazgas
         * @param prev prieš tai buvęs(previous) mazgas
         */
        Node(E e, Node<E> next, Node<E> prev) {
            this.element = e;
            this.next = next;
            this.prev = prev;
        }

        /**
         * Suranda sąraše k-ąjį mazgą
         *
         * @param k ieškomo mazgo indeksas (prasideda nuo 0)
         * @return surastas mazgas
         */
        public Node<E> findNode(int k) {
            Node<E> e = this;
            for (int i = 0; i < k; i++) {
                e = e.next;
            }
            return e;
        }

    } // klasės Node pabaiga

    /**
     * Metodas nustato preikšmę pasirinktam mazgui
     *
     * @param k pasirinkto mazgo indeksas
     * @param e norima nustatyti reikšmė
     * @return pasalindas node'as
     */
    @Override
    public void set(int k, E e) {

        if (k < 0 || k >= size) {
            throw new IndexOutOfBoundsException("Indeksas netelpa į rėžius");
        }

        Node<E> l = first.findNode(k);
        l.element = e;
    }

    /**
     * Metodas pašalinti elementą iš sąrašo
     *
     * @param k šalinamo elemento indeksas
     * @return
     */
    @Override
    public int remove(int k) {
        Node<E> darb = null;

        if (k < 0 || k >= size) {
            throw new IndexOutOfBoundsException("Indeksas netelpa į rėžius");
        } 
        
        /* jeigu vienas elementas tiesiog nunuliname */
        if (size == 1) {
            first = null;
            last = null;
            size = 0;
            return 0;    
        }

        if (k == 0) {
            first = first.next;
            darb = first;
            darb.prev = null; // ?
        } else if (k == size) {
            last = last.prev;
            darb = last;
            darb.next = null;  // ?
        } else {
            darb = first.findNode(k).prev;
            darb.next = darb.next.next;
            if (darb.next == null) {
                last = darb;
            }
        }
        
        size--;
        return 0;
    }

    /**
     * Tikrinimas ar e elementas jau egzituoja sarase
     *
     * @param e objektas
     * @return true, jeigu egzistuoja
     */
    @Override
    public boolean Contains(E e) {
        for (Node<E> i = first; i != null; i = i.next) {
            if (i.element.equals(e)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Gauti indeksa pirmo tokio objekto sarase
     *
     * @param objektas
     * @return jo vieta sarase
     */
    @Override
    public int getIndex(E e) {
        int k = 0;

        if (e == null) {
            throw new NullPointerException("Objektas yra neapibrėžtas");
        }

        for (Node<E> i = first; i != null; i = i.next) {
            if (i.element.equals(e)) {
                return k;
            }
            k++;
        }
        return -1;
    }

    /**
     * Metodas išspausdina į teminalą visas reiksmes sąraše
     */
    @Override
    public String printAll() {
        String finalString = "";
        
        if (size == 0)
            return "Sąrašas neturi elementų\n";
        
        for (Node<E> i = first; i != null; i = i.next) {
            finalString += i.element + "\n";
        }
        return finalString;
    }
}
