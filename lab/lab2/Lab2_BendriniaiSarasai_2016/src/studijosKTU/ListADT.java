/**
 * @author Kasparas Zulonas KTU IFIN-8/3
 */
package studijosKTU;

public interface ListADT<E> {

	/**
	 * Appends the specified element to the end of this list.
	 *
	 * @param e element to be appended to this list
	 * @return true, if operation is Ok
	 */
	boolean addLast(E e);
        
        
        /**
         * Metodas add įdeda elementą į sąrašo pražią
         *
         * @param e - tai įdedamas elementas (jis negali būti null)
         * @return true, jei operacija atlikta sėkmingai
         */
        boolean addFirst(E e);

	/**
	 * Returns the number of elements in this list.
	 */
	int size();

	/**
	 * @return true if this list contains no elements.
	 */
	boolean isEmpty();

	/**
	 * Removes all of the elements from this list.
	 */
	void clear();

	/**
	 * Returns the element at the specified position in this list.
	 *
	 * @param k index of the element to return
	 * @return the element at the specified position in this list
	 * @throws IndexOutOfBoundsException {@inheritDoc}
	 */
	E get(int k);

	/**
	 * Atitinka iteratoriaus metodą next (List tokio metodo neturi)
	 * @return kitą reikšmę.
	 */
	E getNext();
        
        /**
         * Metodas gražina prieš tai buvusią reikšmę
         * @return 
         */
        E getPrev();
	
	/**
	 * Returns an array containing all of the elements in this list in proper sequence (from first to last element).
	 * @return an array containing all of the elements
	 */
	Object[] toArray();
        
        /*-------------------------------------------------*/

        /**
         * @param k indeksas
         * @param e reikme
         * @return 
         */
        void set(int k, E e);
        
        /**
         * @param k index
         * @return 
         */
        int remove(int k);
        
        /**
         * @param e tikrinti ar elementas egzistuoja sarase
         * @return true, jeigu taip
         */
        boolean Contains(E e);
        
        /**
         * @param o objektas
         * @return objekto indeksas
         */
        int getIndex(E e);
        
        /**
         * Spausgina visas sukauptas reiksmes
         */
        String printAll();
}
